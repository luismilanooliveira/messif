/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messif.objects.impl;

import java.io.IOException;
import java.util.Collection;
import junit.framework.TestCase;
import messif.objects.DistanceFunction;
import messif.objects.LocalAbstractObject;
import messif.objects.MetaObject;

/**
 *
 * @author david
 */
public class MetaObjectFixedMapTest extends TestCase {
    
    public MetaObjectFixedMapTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    /**
     * Test of getObject method, of class MetaObjectFixedMap.
     */
    public void testGetObject() throws IOException {
        ObjectFloatVectorL2 object1 = new ObjectFloatVectorL2(new float [] {1f, 2f});
        ObjectFloatVectorL2 object2 = new ObjectFloatVectorL2(new float [] {1f, 2f});
        MetaObjectFixedMap metaObjectFixedMap = new MetaObjectFixedMap(new String [] {"caffe", "other"}, new LocalAbstractObject[] {object1, object2}, "key");
        
        ObjectString objStr = new ObjectString("string");
        MetaObjectFixedMap changed = new MetaObjectFixedMap(metaObjectFixedMap, objStr, "caffe");
        changed.write(System.out);
    }
    
}
