/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operations.query;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.operations.AbstractOperation;
import messif.operations.AnswerType;
import messif.operations.QueryOperation;

/**
 * This operation ranks given list of IDs stored in the index with respect to given query object
 *  and return the "k" best objects.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
@AbstractOperation.OperationName("reranking query for a given list of object IDs and internal query object")
public class KNNRankByLocatorOperation extends LocatorQueryOperation {

    /** Class serial id for serialization */
    private static final long serialVersionUID = 1L;

    //****************** Attributes ******************//
    
    /** Number of nearest objects to retrieve */
    protected final int k;

    /** List of locators of existing objects that should be used to re-rank and create the final answer set. */
    protected final List<String> objLocators;
    

    //****************** Constructors ******************//

    /**
     * Creates a new instance of kNNQueryOperation for a given query object and maximal number of objects to return.
     * Objects added to answer are updated to {@link AnswerType#NODATA_OBJECTS no-data objects}.
     * @param queryLocator locator of the object to which the nearest neighbors are searched
     * @param k the number of nearest neighbors to retrieve
     * @param objLocators array of String IDs (locators) of data objects to be reranked
     * @param answerType the type of objects this operation stores in its answer
     */
    @AbstractOperation.OperationConstructor({"Query object locator", "Number of nearest objects", "list of data object IDs to rerank", "type of answer"})
    public KNNRankByLocatorOperation(String [] queryLocator, int k, String objLocators[], AnswerType answerType) {
        super(new HashSet<>(Arrays.asList(queryLocator)), k, answerType);
        this.k = k;
        this.objLocators = Arrays.asList(objLocators);
    }

    /**
     * Creates a new instance of kNNQueryOperation for a given query object and maximal number of objects to return.
     * Objects added to answer are updated to {@link AnswerType#NODATA_OBJECTS no-data objects}.
     * @param queryLocator locator of the object to which the nearest neighbors are searched
     * @param k the number of nearest neighbors to retrieve
     * @param objLocators array of String IDs (locators) of data objects to be reranked
     */
    @AbstractOperation.OperationConstructor({"Query object locator", "Number of nearest objects", "list of data object IDs to rerank"})
    public KNNRankByLocatorOperation(String [] queryLocator, int k, String objLocators[]) {
        this(queryLocator, k, objLocators, AnswerType.NODATA_OBJECTS);
    }

    //****************** Attribute access ******************//

    /**
     * Returns the number of nearest objects to retrieve.
     * @return the number of nearest objects to retrieve
     */
    public int getK() {
        return k;
    }

    /**
     * Returns the list of IDs identifying the objects to ranked.
     * @return 
     */
    public List<String> getObjLocators() {
        return objLocators;
    }
    
    /**
     * Returns argument that was passed while constructing instance.
     * If the argument is not stored within operation, <tt>null</tt> is returned.
     * @param index index of an argument passed to constructor
     * @return argument that was passed while constructing instance
     * @throws IndexOutOfBoundsException if index parameter is out of range
     */
    @Override
    public Object getArgument(int index) throws IndexOutOfBoundsException {
        switch (index) {
        case 0:
            return getLocators();
        case 1:
            return k;
        case 2:
            return objLocators;
        default:
            throw new IndexOutOfBoundsException("kNNQueryOperation has only two arguments");
        }
    }

    /**
     * Returns number of arguments that were passed while constructing this instance.
     * @return number of arguments that were passed while constructing this instance
     */
    @Override
    public int getArgumentCount() {
        return 3;
    }

    /**
     * Limit the length of the string representation of the list of IDs down to 100.
     * @param index index of the parameter to return the string for
     * @return string representation of given parameter; the behavior is changed just for index=2
     * @throws IndexOutOfBoundsException if the index is >= the number of arguments
     * @throws UnsupportedOperationException if the String representation of the param is not available
     */
    @Override
    public String getArgumentString(int index) throws IndexOutOfBoundsException, UnsupportedOperationException {
        if (index == 2) {
            StringBuilder listOfLocators = new StringBuilder(String.valueOf(getArgument(index)));
            if (listOfLocators.length() > 100) {
                return listOfLocators.delete(100, listOfLocators.length()).append("... (size: ").append(objLocators.size()).append(") ]").toString();
            }
            return listOfLocators.toString();
        }
        return super.getArgumentString(index);
    }
    

    //****************** Implementation of query evaluation ******************//

    /**
     * Evaluate this query on a given set of objects.
     * The objects found by this evaluation are added to answer of this query via {@link #addToAnswer}.
     *
     * @param objects the collection of objects on which to evaluate this query
     * @return number of objects satisfying the query
     */
    @Override
    public int evaluate(AbstractObjectIterator<? extends LocalAbstractObject> objects) {
        Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "running evaluate on {0}: {1}", new Object[]{KNNRankByLocatorOperation.class.toString(), getLocators()});
        return 0;
    }


    //****************** Equality driven by operation data ******************//

    /** 
     * Indicates whether some other operation has the same data as this one.
     * @param   obj   the reference object with which to compare.
     * @return  <code>true</code> if this object has the same data as the obj
     *          argument; <code>false</code> otherwise.
     */
    @Override
    protected boolean dataEqualsImpl(QueryOperation obj) {
        // The argument obj is always kNNQueryOperation or its descendant, because it has only abstract ancestors
        KNNRankByLocatorOperation castObj = (KNNRankByLocatorOperation)obj;

        if (getLocators().equals(castObj.getLocators()))
            return false;

        return k == castObj.k;
    }

    /**
     * Returns a hash code value for the data of this operation.
     * @return a hash code value for the data of this operation
     */
    @Override
    public int dataHashCode() {
        return (getLocators().hashCode() << 8) + k + objLocators.hashCode() >> 1;
    }

}
